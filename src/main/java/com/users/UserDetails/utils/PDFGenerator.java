package com.users.UserDetails.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.users.UserDetails.model.User;

public class PDFGenerator {

	public static void createPDF(List<User> users) throws FileNotFoundException, DocumentException {

		Document document = new Document();

		PdfPTable table = new PdfPTable(new float[] { 2, 2, 2, 2, 2, 2, 2 });

		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

//                            Column names : cell means one block

		table.addCell("Id");
		table.addCell("FirstName");
		table.addCell("LastName");
		table.addCell("Age");
		table.addCell("Address");
		table.addCell("Occupation");
		table.addCell("UserName");

//                            Header row of the cell

		table.setHeaderRows(1);

		PdfPCell[] cells = table.getRow(0).getCells();

		for (int j = 0; j < cells.length; j++) {

			cells[j].setBackgroundColor(BaseColor.GRAY);

		}

//                            Get the item from the db in list : users

//                            iterate at this place

		for(User user:users) {
			
			table.addCell(((Long)user.getId()).toString());
			table.addCell(user.getFirstName());
			table.addCell(user.getLastName());
			table.addCell(((Integer)user.getAge()).toString());
			table.addCell(user.getAddress());
			table.addCell(user.getOccupation());
			table.addCell(user.getUserName());
		}

		PdfWriter.getInstance(document, new FileOutputStream("userList.pdf"));

		document.open();

		document.add(table);

		document.close();

		System.out.println("Done");

	}

}
