package com.users.UserDetails.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.users.UserDetails.businessService.UserBusinessService;
import com.users.UserDetails.model.User;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserBusinessService userBusinessService;

	@PostMapping("createUser")
	public User createUser(@RequestBody User user) {

		user = userBusinessService.createUser(user);

		return user;

	}

	@GetMapping
	public List<User> getUsers() {

		List<User> users = userBusinessService.getUsers();

		return users;

	}

	@GetMapping("userName/{userName}")
	public User getUserByUsername(@PathVariable("userName") String userName) {

		User user = userBusinessService.getUserByUsername(userName);

		return user;

	}

	@GetMapping( "/pdfreport")
	public String userReport() {
		System.out.println("pdf report started...");
		userBusinessService.genertePDF();

		return "PDF CREATED...";
	}
}
