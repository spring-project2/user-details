package com.users.UserDetails.businessService;

import java.io.FileNotFoundException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.DocumentException;
import com.users.UserDetails.model.User;
import com.users.UserDetails.repository.UserRepository;
import com.users.UserDetails.utils.PDFGenerator;

@Service
public class UserBusinessService {

	@Autowired
	UserRepository userRepository;

	public User createUser(User user) {

		user = userRepository.save(user);

		return user;
	}

	public List<User> getUsers() {

		List<User> usersList = userRepository.findAll();

		return usersList;
	}

	public User getUserByUsername(String userName) {

		User user = userRepository.findByUserName(userName);

		return user;
	}

	public List<User> getAllDetails() {

		return (List<User>) userRepository.findAll();

	}

	public void genertePDF() {
		
		List<User> users = userRepository.findAll();
		System.out.println(users);
		if(users!= null) {
			try {
				PDFGenerator.createPDF(users);
			} catch (FileNotFoundException | DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
	}
}
