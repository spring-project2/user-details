package com.users.UserDetails.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.users.UserDetails.businessService.UserBusinessService;
import com.users.UserDetails.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	//UserBusinessService save(UserBusinessService user);

	User findByUserName(String userName);

}
