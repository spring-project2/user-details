drop table if exists USER;
CREATE TABLE `USER` (
 `ID` int(11) NOT NULL,
 `FIRST_NAME` varchar(20) NOT NULL,
 `LAST_NAME` varchar(20) NOT NULL,
 `USER_NAME` varchar(20) NOT NULL,
 `AGE` int(11) NOT NULL,
 `OCCUPATION` varchar(30) NOT NULL,
 `ADDRESS` varchar(100) NOT NULL
);
